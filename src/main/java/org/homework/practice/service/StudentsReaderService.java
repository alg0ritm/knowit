package org.homework.practice.service;

import org.homework.practice.model.Student;

import java.util.List;

public interface StudentsReaderService {

    public List<Student> obtainStudents();
}
