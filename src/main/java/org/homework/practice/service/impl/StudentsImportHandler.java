package org.homework.practice.service.impl;

import java.util.List;

import lombok.Data;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.homework.practice.model.Student;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import static com.google.common.collect.Lists.newArrayList;

@Data
public class StudentsImportHandler extends DefaultHandler {

    private static final String PARENT = "Students";
    private static final String STUDENT = "student";
    private static final String STUDENT_NAME = "name";
    private static final String STUDENT_MATH = "math_grade";
    private static final String STUDENT_PHYS = "physics_grade";

    private List<Student> grades = newArrayList();

    @Override
    public void startElement(String uri, String localName, String xmlElement, Attributes attributes) throws SAXException {
        if (xmlElement.equalsIgnoreCase(STUDENT)) {
            if (isCorrectLine(attributes)) {
                grades.add(new Student(calculateWeightedAverageMean(Integer.valueOf(attributes.getValue(STUDENT_MATH)) + Integer.valueOf(attributes.getValue(STUDENT_PHYS))), attributes.getValue(STUDENT_NAME)));
            }
        }
        super.startElement(uri, localName, xmlElement, attributes);
    }

    private Double calculateWeightedAverageMean(Integer sumOfGrades) {
        return sumOfGrades/2D;
    }

    private boolean isCorrectLine(Attributes attributes) {
        return attributes.getValue(STUDENT_NAME) != null
                && attributes.getValue(STUDENT_MATH) != null
                && attributes.getValue(STUDENT_PHYS) != null;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase(PARENT)) {
            return;
        }
    }
}
