package org.homework.practice.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.homework.practice.model.Student;
import org.homework.practice.service.StudentsReaderService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

@Service
public class StudentsReaderServiceImpl implements StudentsReaderService {

    private static final Log LOG = LogFactory.getLog(StudentsReaderServiceImpl.class);

    @Value("${path.to.students:students.xml}")
    private String studentsFileName;

    @Override
    public List<Student> obtainStudents() {
        try {
            StudentsImportHandler currencyRateHandler = new StudentsImportHandler();
            SAXParserFactory mySAXParserFactory = SAXParserFactory.newInstance(); //just in case use sax parser
            SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
            XMLReader myXMLReader = mySAXParser.getXMLReader();
            myXMLReader.setContentHandler(currencyRateHandler);
            mySAXParser.parse(new File(studentsFileName), currencyRateHandler);
            return currencyRateHandler.getGrades();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOG.error(e);
        }
        return newArrayList(); //avoid nulls;
    }
}
