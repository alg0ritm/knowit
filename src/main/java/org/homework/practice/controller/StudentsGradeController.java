package org.homework.practice.controller;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Ordering;
import org.homework.practice.model.Student;
import org.homework.practice.service.StudentsReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.*;

@Controller
public class StudentsGradeController {

    @Autowired
    private StudentsReaderService studentsReaderService;

    @VisibleForTesting
    protected static final Ordering<Student> ORDER_BY_WEIGHTED_AVERAGE_MEAN_GRADE_AND_NAME = new Ordering<Student>() {
        @Override public int compare(Student left, Student right) {
            int basedOnlyOnWeightedAverageMean = orderByWeightedAverageMean(left, right);
            if(averageMeansAreEqual(basedOnlyOnWeightedAverageMean)) return orderByName(left, right);
            return basedOnlyOnWeightedAverageMean;
        }

        private int orderByWeightedAverageMean(Student left, Student right) {
            return right.getWeightedAverageMean().compareTo(left.getWeightedAverageMean());
        }

        private int orderByName(Student left, Student right) {
            return left.getName().compareTo(right.getName());
        }

        private boolean averageMeansAreEqual(int basedOnlyOnWeightedAverageMean) {
            return basedOnlyOnWeightedAverageMean == 0;
        }
    };

    @RequestMapping(value = "/students", method = RequestMethod.GET)
    public String init(Model model) {
        model.addAttribute("students", ORDER_BY_WEIGHTED_AVERAGE_MEAN_GRADE_AND_NAME.sortedCopy(studentsReaderService.obtainStudents()));
        return "students";
    }
}
