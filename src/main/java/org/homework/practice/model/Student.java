package org.homework.practice.model;

import lombok.Data;

@Data
public class Student {
    private Double weightedAverageMean;
    private String name;

    public Student(Double weightedAverageMean, String name) {
        this.weightedAverageMean = weightedAverageMean;
        this.name = name;
    }
}
