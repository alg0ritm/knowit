package org.homework.practice.controller;

import com.google.common.collect.ImmutableList;
import org.homework.practice.model.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class StudentsGradeControllerTest {

    @Test
    public void studentsAreOrderedInDescendingOrder() {
        List<Student> students = ImmutableList.of(
                new Student(5.5, "Anton"),
                new Student(5.6, "Nikolai"),
                new Student(5.7, "Elvis"),
                new Student(5.8, "Danila"),
                new Student(5.9, "Peeter"));

        List<Student> test = StudentsGradeController.ORDER_BY_WEIGHTED_AVERAGE_MEAN_GRADE_AND_NAME.sortedCopy(students);

        for (int i = 0; i < test.size() ; i++) {
            assertThat(students.get(i).getName()).isEqualTo(test.get(test.size()-1-i).getName());
            assertThat(students.get(i).getWeightedAverageMean()).isEqualTo(test.get(test.size()-1-i).getWeightedAverageMean());
        }
    }

    @Test
    public void studentsAreByNameInCaseOfSameWeightedMeans() {
        List<Student> students = ImmutableList.of(
                new Student(5.5, "Anton"), //1
                new Student(5.5, "Nikolai"), //4
                new Student(5.5, "Elvis"), //3
                new Student(5.5, "Danila"), //2
                new Student(5.5, "Peeter")); //5

        List<Student> test = StudentsGradeController.ORDER_BY_WEIGHTED_AVERAGE_MEAN_GRADE_AND_NAME.sortedCopy(students);

        assertThat(students.get(0).getName()).isEqualTo(test.get(0).getName());
        assertThat(students.get(0).getWeightedAverageMean()).isEqualTo(test.get(0).getWeightedAverageMean());

        assertThat(students.get(1).getName()).isEqualTo(test.get(3).getName());
        assertThat(students.get(1).getWeightedAverageMean()).isEqualTo(test.get(3).getWeightedAverageMean());

        assertThat(students.get(2).getName()).isEqualTo(test.get(2).getName());
        assertThat(students.get(2).getWeightedAverageMean()).isEqualTo(test.get(2).getWeightedAverageMean());

        assertThat(students.get(3).getName()).isEqualTo(test.get(1).getName());
        assertThat(students.get(3).getWeightedAverageMean()).isEqualTo(test.get(1).getWeightedAverageMean());

        assertThat(students.get(4).getName()).isEqualTo(test.get(4).getName());
        assertThat(students.get(4).getWeightedAverageMean()).isEqualTo(test.get(4).getWeightedAverageMean());
    }

}