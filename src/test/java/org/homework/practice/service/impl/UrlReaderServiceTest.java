package org.homework.practice.service.impl;

import org.homework.practice.model.Student;
import org.homework.practice.service.StudentsReaderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class UrlReaderServiceTest {

    @Autowired
    private StudentsReaderService urlReaderService;

    @Test
    public void studentsAreObtainedFromXmlFileAssociatedWithTestContext() {
        List<Student> students = urlReaderService.obtainStudents();
        assertThat(students).hasSize(6);
    }
}
